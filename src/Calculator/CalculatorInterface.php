<?php


namespace Calculator;

/**
 * Interface CalcInterface
 */
interface CalculatorInterface
{
    /**
     * @return array
     */
    public function getPricesData();
}
