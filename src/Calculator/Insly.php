<?php
namespace Calculator;

/**
 * Class Insly
 */
class Insly implements CalculatorInterface
{
    /** constants for calculate percentage  */
    const TOTAL_COMISSION_PERCENT = 17;
    const BASE_PREMIUM_PERCENT = 11;
    const TAX_PERCENT = 10;

    /** @var int */
    private $basePrice;

    /** @var int */
    private $taxPercentage;

    /** @var int */
    private $instalmentsNumber;

    /**
     * Run constructor.
     * @param int $basePrice
     * @param int $taxPercentage
     * @param int $instalmentsNumber
     *
     * @return CalculatorInterface
     */
    public function __construct($basePrice, $taxPercentage, $instalmentsNumber)
    {
        $this->basePrice = $basePrice;
        $this->taxPercentage = $taxPercentage;
        $this->instalmentsNumber = $instalmentsNumber;
    }

    /**
     * @return array
     * @throws \Exception
     */
    public function getPricesData()
    {
        $totalBasePremiumSum = $this->getTotalBasePremium();
        $comission = $totalBasePremiumSum * self::TOTAL_COMISSION_PERCENT / 100;
        $tax = $totalBasePremiumSum * $this->taxPercentage / 100;
        $totalCost = $totalBasePremiumSum + $comission + $tax;
        $result = [
            'base' => [
                'value' => self::formatPrice($this->basePrice),
                'total_base_premium' => self::formatPrice($totalBasePremiumSum),
                'comission' => self::formatPrice($comission),
                'tax' => self::formatPrice($tax),
                'total_cost' => self::formatPrice($totalCost),
                'base_premium' => $this->getBasePremiumPercent(),
                'comission_percent' => self::TOTAL_COMISSION_PERCENT,
                'tax_percent' => self::TAX_PERCENT
            ]
        ];

        if ($this->instalmentsNumber > 1) {
            $firstChunkTotalCost = $this->getFirstChunk($totalCost);
            $firstChunkTax = $this->getFirstChunk($tax);
            $firstChunkComission = $this->getFirstChunk($comission);
            $firstChunkBasePremium = $this->getFirstChunk($totalBasePremiumSum);

            $result['instalmentsNumber'] = $this->instalmentsNumber;
            $result['chunk'] = [
                'total_base_premium' => self::formatPrice($totalBasePremiumSum / $this->instalmentsNumber),
                'comission' => self::formatPrice($comission / $this->instalmentsNumber),
                'tax' => self::formatPrice($tax / $this->instalmentsNumber),
                'total_cost' => self::formatPrice($totalCost / $this->instalmentsNumber),
            ];
            $result['first_chunk_total_cost'] = self::formatPrice($firstChunkTotalCost);
            $result['first_chunk_tax'] = self::formatPrice($firstChunkTax);
            $result['first_chunk_comission'] = self::formatPrice($firstChunkComission);
            $result['first_chunk_base_premium'] = self::formatPrice($firstChunkBasePremium);
        }

        return $result;
    }

    /**
     * @param float $value
     * @return float
     */
    private function getFirstChunk($value)
    {
        $differentSum = round($value - $this->instalmentsNumber * round($value / $this->instalmentsNumber, 2), 2);
        $firstChunk = $value / $this->instalmentsNumber;
        if ($differentSum !== 0) {
            $firstChunk = round((round(
                $value - $this->instalmentsNumber * round($value / $this->instalmentsNumber, 2), 2) +
                $value / $this->instalmentsNumber)
                , 2
            );
        }

        return $firstChunk;
    }

    /**
     * @return float
     * @throws \Exception
     */
    private function getTotalBasePremium()
    {
        return $this->basePrice  * $this->getBasePremiumPercent() / 100;
    }

    /**
     * @return int
     * @throws \Exception
     */
    private function getBasePremiumPercent()
    {
        $dateTime = new \DateTime();
        $currentHourTime = $dateTime->format('H');
        $basePremium = self::BASE_PREMIUM_PERCENT;
        if ($dateTime->format('D') === 'Fri' && ($currentHourTime > 15) && ($currentHourTime < 20)) {
            $basePremium = 13;
        }

        return $basePremium;
    }

    /**
     * @param int $price
     * @return string
     */
    private static function formatPrice($price)
    {
        return number_format($price, 2, '.', '');
    }
}
