<?php
require_once 'vendor/autoload.php';
?>
<html lang="en">
    <head>
        <title>Simple Tax Calculator(Task 2)</title>
    </head>
    <body>
        <h1>Tax calculator</h1>
        <form class="c-form">
            <table>
                <tr>
                    <td>Enter price value</td>
                    <td><input type="text" class="priceValue" value="10000" /></td>
                </tr>
                <tr>
                    <td>Set tax percentage</td>
                    <td>
                        <input type="range" name="taxPercentage" class="taxPercentage slider" value="20" min="1" max="100" step="1" />
                        <span class="taxPercentageValue">20</span>
                    </td>
                </tr>
                <tr>
                    <td>Set instalments number</td>
                    <td>
                        <input type="range" name="instalmentsNumber" class="instalmentsNumber slider" value="2" min="1" max="12" step="1" />
                        <span class="instalmentsNumberValue">2</span>
                    </td>
                </tr>
                <tr>
                    <td><input type="button" class="calcButton" value="Make calculation!" /></td>
                </tr>
            </table>
        </form>
        <script type="text/javascript">
            var url = "<?php echo $_SERVER['REQUEST_URI'] . 'ajax.php'; ?>";
        </script>
        <script
                src="https://code.jquery.com/jquery-3.4.1.min.js"
                integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="
                crossorigin="anonymous"></script>
        <script src="web/script.js"></script>
        <link rel="stylesheet" type="text/css" href="web/styles.css" >
    </body>
</html>
