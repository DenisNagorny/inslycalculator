<?php
require_once 'vendor/autoload.php';

use Calculator\Insly;

$priceValue = (int) $_POST['priceValue'];
$taxPercentage = (int) $_POST['taxPercentage'];
$instalmentsNumber = (int) $_POST['instalmentsNumber'];
$sumObject = new Insly($priceValue, $taxPercentage, $instalmentsNumber);
try {
    echo json_encode($sumObject->getPricesData());
} catch (Exception $e) {
    echo $e->getMessage();
}
