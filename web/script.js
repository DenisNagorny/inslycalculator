jQuery(document).ready(function($) {
    $(document).on('input', '.taxPercentage', function() {
        $('.taxPercentageValue').html( $(this).val() );
    });

    $(document).on('input', '.instalmentsNumber', function() {
        $('.instalmentsNumberValue').html( $(this).val() );
    });

    $('.calcButton').on('click', function () {
        var priceValue = $('.priceValue').val(),
            taxPercentage = $('.taxPercentage').val(),
            instalmentsNumber = $('.instalmentsNumber').val();

        $.ajax({
            url: url,
            type: "POST",
            data: {
                priceValue: priceValue,
                taxPercentage: taxPercentage,
                instalmentsNumber: instalmentsNumber
            },
            cache: false,
            showLoader: false,
            dataType: 'json',
            success: function (result) {
                var html = '<table class="result-table" border="1"><tr><td></td><td>Policy</td>';
                for (var i = 1; i <= instalmentsNumber; i++) {
                    html += '<td>' + i + ' instalment' + '</td>';
                }
                html += '</tr>';

                html += '<tr><td>Value</td><td>' + result.base.value +'</td>';
                for (i = 1; i <= instalmentsNumber; i++) {
                    html += '<td></td>';
                }
                html += '</tr>';

                html += '<tr><td>Base premium ' + result.base.base_premium  + '%</td>';
                html += '<td>' + result.base.total_base_premium + '</td>';
                for (i = 1; i <= instalmentsNumber; i++) {
                    if (i == 1) {
                        html += '<td>' + result.first_chunk_base_premium + '</td>';
                        continue;
                    }
                    html += '<td>' + result.chunk.total_base_premium + '</td>';
                }
                html += '</tr>';

                html += '<tr><td>Comission ' + result.base.comission_percent  + '%</td>';
                html += '<td>' + result.base.comission + '</td>';
                for (i = 1; i <= instalmentsNumber; i++) {
                    if (i == 1) {
                        html += '<td>' + result.first_chunk_comission + '</td>';
                        continue;
                    }
                    html += '<td>' + result.chunk.comission + '</td>';
                }
                html += '</tr>';

                html += '<tr><td>Tax ' + result.base.tax_percent  + '%</td>';
                html += '<td>' + result.base.tax + '</td>';
                for (i = 1; i <= instalmentsNumber; i++) {
                    if (i == 1) {
                        html += '<td>' + result.first_chunk_tax + '</td>';
                        continue;
                    }
                    html += '<td>' + result.chunk.tax + '</td>';
                }
                html += '</tr>';

                html += '<tr><td>Total cost</td>';
                html += '<td>' + result.base.total_cost + '</td>';
                for (i = 1; i <= instalmentsNumber; i++) {
                    if (i == 1) {
                        html += '<td>' + result.first_chunk_total_cost + '</td>';
                        continue;
                    }
                    html += '<td>' + result.chunk.total_cost + '</td>';
                }
                html += '</tr>';

                html += '</table>';
                $('.result-table').remove();
                $(html).insertAfter('.c-form');
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log('jqXHR', jqXHR);
                console.log('textStatus', textStatus);
                console.log('e', errorThrown);
            }
        });
    });
});
